import { Moment, Duration } from "moment";
export declare namespace OData {
    interface Response<T> {
        value: T[];
    }
    namespace Legacy {
        type _Members = {
            UserId: string;
            UserType: string;
            UserName: string;
            Name: string;
            Email: string;
            IsASite: boolean;
        };
        type _MembersOptional = Partial<{
            CompanyId: string;
            Latitude: number;
            Longitude: number;
            TimeZone: string;
            ExpiryDate: Date;
            LastActivityDate: Date;
            CreateDate: Date;
            LastLoginDate: Date;
            LastLockoutDate: Date;
            LastPasswordChangedDate: Date;
            FailedPasswordAttemptCount: number;
            FailedPasswordAttempWindowStart: Date;
            IsLockedOut: boolean;
            MediaId: string;
            UserTypeId: string;
            FSSiteId: string;
            DefaultLanguageCode: string;
            MembershipLabels: MembersLabel[];
            MembershipContexts: any;
            FSSite: any;
        }>;
        type Members = _Members & _MembersOptional;
        interface Label {
            Id: Guid;
            Name: string;
            LabelGroup: string;
            Forecolor: number;
            Backcolor: number;
        }
        interface MembersLabel extends Label {
        }
        interface RosterLabel extends Label {
        }
        interface TaskLabel {
            LabelId: Guid;
            Name: string;
            LabelGroup: string;
            Forecolor: number;
            Backcolor: number;
        }
        interface TaskTypeLabel extends TaskLabel {
        }
        interface Task {
            Id: string;
            RosterId: number;
            GroupId: Guid | null;
            Roster: string;
            TaskTypeId: Guid;
            TaskType: string;
            DateCreated: Moment;
            DateStarted: Moment | null;
            DateCompleted: Moment | null;
            DaysSinceCompletion: number | null;
            Name: string;
            Description: string | null;
            OwnerId: Guid;
            Owner: string;
            OwnerCompanyId: Guid;
            OriginalOwnerId: Guid;
            OriginalOwner: string;
            SiteId: Guid | null;
            SiteName: string | null;
            SiteLatitude: number | null;
            SiteLongitude: number | null;
            RoleId: number | null;
            Role: string | null;
            Level: number;
            CompletionNotes: string | null;
            Status: number;
            StatusDate: Moment;
            StatusText: string;
            HierarchyBucketId: number | null;
            CustomStatusId: Guid | null;
            CustomStatusText: string | null;
            ActualDuration: number | null;
            InactiveDuration: number | null;
            WorkingDocumentCount: number;
            AllActivitiesEstimatedDuration: number;
            CompletedActivitiesEstimatedDuration: number;
            StartTime: Moment | null;
            LateAfter: Moment | null;
            ScheduledOwnerId: Guid | null;
            ScheduledOwner: string | null;
            EstimatedDurationSeconds: number | null;
            OriginalTaskTypeId: Guid | null;
            OriginalTaskType: string | null;
            ProjectJobScheduledTaskId: Guid | null;
            HasOrders: boolean;
            AssetId: number | null;
            EndsOn: Moment;
            FSSiteId: number | number;
            OwnerTypeId: number;
            OriginalOwnerTypeId: number;
            ScheduledOwnerTypeId: number | null;
            SiteTypeId: number | null;
            SortOrder: number;
            LastModifiedDateUtc: Moment;
            StaffingLevel: number;
            RosterStaffingLevel: number;
            InPlay: number;
            EstimatedSecondsToStart: number | null;
            CreatedByUserId: Guid | null;
            TaskLabels: TaskLabel[];
            TaskFinishStatus: any;
            TaskWorklogs: any;
            OwnerCompany: any;
            Asset: any;
            HierarchyBucket: any;
            FSSite: any;
            RosterRecord: any;
            OwnerRecord: Members | null;
            Membership: Members | null;
            CreatedByUser: Members | null;
        }
        interface GlobalSetting {
            Id: number;
            Name: string;
            Value: string;
        }
        interface TaskType {
            Id: Guid;
            Name: string;
            UserInterfaceType: number;
            MediaId: Guid;
            DefaultLengthInDays: number;
            AutoStart1stChecklist: boolean;
            PhotoSupport: number;
            AutoFinishAfterLastChecklist: boolean;
            EstimatedDurationSeconds: number;
            ProductCatalogs: any;
            MinimumDuration: number | null;
            ExcludeFromTimesheets: boolean;
            AutoFinishAfterStart: boolean;
            ChecklistCount: number;
            Description: string | null;
            EstimatedSecondsToStart: number | null;
            KpiDataPoints: any;
            TaskTypeLabels: TaskTypeLabel[];
        }
    }
}
export declare type Guid = string;
export interface IHasLabels {
    /** Explicit labels to add (use Label Id or unique name of Label) */
    labels?: Guid[];
}
export interface IHasName {
    readonly name: string;
}
export interface IHasId<TId extends number | Guid> {
    /** Id of this record */
    readonly id: TId;
}
export interface ICreatedByUser {
    /** The userId responsible for creating this record */
    readonly createdByUserId: Guid;
}
export declare type PrepareType = 'cached' | // This is called after the Checklist has been downloaded
'pre-run' | // This is called immediately after a new Checklist has been prepped to run
'pre-restore' | // This is called immediately after a Checklist has been restored to run
'dispose' | // This is called when the checklist is being disposed completely
'before-replace';
export interface IPrepareable {
    prepare?(prepareType: PrepareType): Promise<void> | void;
}
export interface IFacility extends IPrepareable {
    readonly facilityType: FacilityType;
}
export interface IReadRecordFacility<TId extends number | Guid, TRecord extends IHasId<TId> & IHasName> extends IFacility {
    get(id: TId): Promise<TRecord | undefined>;
    getByName(name: string): Promise<TRecord | undefined>;
    getAll(): Promise<TRecord[] | undefined>;
}
export interface IOfflineCreateResult<TRecord> {
    /** This is the Guid that is generated offline, upon success the resulting record will have the same id */
    readonly id: Guid;
    /** This is the result from the server on success */
    readonly result: Promise<TRecord>;
}
export declare type IOfflineUpdateResult<TRecord> = Promise<TRecord>;
/** An OfflineFacility has the ability to create records offline, this requires the Id to be a Guid */
export interface IOfflineFacility<TRecord extends IHasId<Guid>, TCreateRecordDefinition, TUpdateRecordDefinition> extends IFacility {
    get(id: Guid): Promise<TRecord>;
    create(definition: TCreateRecordDefinition): Promise<IOfflineCreateResult<TRecord>>;
    update(definition: TUpdateRecordDefinition): Promise<IOfflineUpdateResult<TRecord>>;
}
export declare type IHandlesFacilityType<T> = Record<FacilityType, T>;
export interface IHandlesQuestionTypes<T> {
    [`checkbox`]: T;
    ['dateTime']: T;
    [`freeText`]: T;
    ['label']: T;
    [`multiChoice`]: T;
    [`number`]: T;
    [`section`]: T;
    ['temperatureProbe']: T;
}
export declare type QuestionType = keyof IHandlesQuestionTypes<void>;
export interface IHtmlPresentationDefinition {
    layout?: number | null;
    alignment?: number | null;
    clear?: number | null;
    customClass?: string | null;
}
export interface IDesignQuestion {
    questionType: QuestionType;
    id: string;
}
export interface IQuestionDefinition extends IHasLabels {
    name?: string;
    htmlPresentation?: IHtmlPresentationDefinition;
    visible?: boolean | null;
    prompt?: string | null;
    hidePrompt?: boolean | null;
    note?: string | null;
    allowNotes?: boolean | null;
    score?: number | null;
}
export interface ICheckboxQuestionDefinition extends IQuestionDefinition {
    defaultAnswer?: boolean | null;
}
export interface IDateTimeQuestionDefinition extends IQuestionDefinition {
    defaultAnswer?: Date | null;
}
export interface IFreeTextQuestionDefinition extends IQuestionDefinition {
    allowMultiline?: boolean | null;
    defaultAnswer?: string | null;
}
export interface ILabelQuestionDefinition extends IQuestionDefinition {
}
export interface IMultiChoiceItemDefinition {
    id: string;
    value: string;
    display?: string;
}
export declare type MultiChoiceDisplayFormat = `List` | `List_Horizontal` | `Compact`;
export interface IMultiChoiceQuestionDefinition extends IQuestionDefinition {
    allowMultiSelect?: boolean | null;
    displayFormat?: MultiChoiceDisplayFormat | null;
    choices?: (IMultiChoiceItemDefinition | string)[] | null;
    defaultAnswer?: IMultiChoiceItemDefinition[] | null;
}
export declare type NumberQuestionKeypadType = `none` | `integer` | `decimal`;
export interface INumberQuestionDefinition extends IQuestionDefinition {
    defaultAnswer?: number | null;
    keypadType?: NumberQuestionKeypadType | null;
    step?: number | null;
}
export declare type TemperatureProbeQuestionReadingType = `realtime` | `maximumReached`;
export interface ITemperatureProbeQuestionDefinition extends IQuestionDefinition {
    defaultAnswer?: number | null;
    readingType?: TemperatureProbeQuestionReadingType;
    displayAsFahrenheit?: boolean | null;
    maximumPassReading?: number | null;
    minimumPassReading?: number | null;
    refreshPeriod?: number | null;
    allowedProbeProviders?: string[] | null;
    allowedProbeSerialNumbers?: string[] | null;
    probeOutOfRangeTimeout?: number | null;
    disconnectProbeTimeout?: number | null;
}
export interface ISectionDefinition extends IQuestionDefinition {
    children?: IQuestionDefinition[] | null;
}
export declare type DesignQuestion = IDesignQuestion & IQuestionDefinition;
export declare type DesignCheckboxQuestion = IDesignQuestion & ICheckboxQuestionDefinition;
export declare type DesignDateTimeQuestion = IDesignQuestion & IDateTimeQuestionDefinition;
export declare type DesignFreeTextQuestion = IDesignQuestion & IFreeTextQuestionDefinition;
export declare type DesignLabelQuestion = IDesignQuestion & ILabelQuestionDefinition;
export declare type DesignMultiChoiceQuestion = IDesignQuestion & IMultiChoiceQuestionDefinition;
export declare type DesignNumberQuestion = IDesignQuestion & INumberQuestionDefinition;
export declare type DesignTemperatureProbeQuestion = IDesignQuestion & ITemperatureProbeQuestionDefinition;
export declare type DesignSection = IDesignQuestion & ISectionDefinition;
export interface IDesignDocumentFacilityDefinition extends IHasLabels {
    /** Explicit name of the DesignDocument, if none is specified it will default to the name of the Checklist in which it is declared */
    name?: string;
    ngController?: string;
}
export interface IDesignDocumentFacility extends IFacility, IHandlesQuestionTypes<(...args: any) => IQuestionDefinition> {
    questions: {
        [name: string]: IQuestionDefinition;
    };
    allSections: ISectionDefinition[];
    sections: {
        [name: string]: ISectionDefinition;
    };
    checkboxTemplate(template: Partial<ICheckboxQuestionDefinition>): Partial<ICheckboxQuestionDefinition>;
    checkbox(definition: ICheckboxQuestionDefinition): DesignCheckboxQuestion;
    dateTimeTemplate(template: Partial<IDateTimeQuestionDefinition>): Partial<IDateTimeQuestionDefinition>;
    dateTime(definition: IDateTimeQuestionDefinition): DesignDateTimeQuestion;
    freeTextTemplate(template: Partial<IFreeTextQuestionDefinition>): Partial<IFreeTextQuestionDefinition>;
    freeText(definition: IFreeTextQuestionDefinition): DesignFreeTextQuestion;
    labelTemplate(template: Partial<ILabelQuestionDefinition>): Partial<ILabelQuestionDefinition>;
    label(definition: ILabelQuestionDefinition): DesignLabelQuestion;
    multiChoiceTemplate(template: Partial<IMultiChoiceQuestionDefinition>): Partial<IMultiChoiceQuestionDefinition>;
    multiChoice(definition: IMultiChoiceQuestionDefinition): DesignMultiChoiceQuestion;
    numberTemplate(template: Partial<INumberQuestionDefinition>): Partial<INumberQuestionDefinition>;
    number(definition: INumberQuestionDefinition): DesignNumberQuestion;
    section(definition: ISectionDefinition): DesignSection;
    temperatureProbeTemplate(template: Partial<ITemperatureProbeQuestionDefinition>): Partial<ITemperatureProbeQuestionDefinition>;
    temperatureProbe(definition: ITemperatureProbeQuestionDefinition): DesignTemperatureProbeQuestion;
}
export interface IGlobalSetting extends IHasId<number>, IHasName {
    /** value of the Global Setting */
    value: string;
}
/** Options to control a GlobalSettingsFacility behaviour */
export interface IGlobalSettingsFacilityDefinition {
    /** A list of Global Setting Names to cache for this checklist */
    names: string[] | null;
}
/** GlobalSettings Facility interface */
export interface IGlobalSettingsFacility extends IReadRecordFacility<number, IGlobalSetting> {
    /** Retrieve a pre-cached GlobalSetting value
     * @returns a Promise resolving to the String value of the GlobalSetting or undefined if not found
     * @example const title = await globalSetting.getString('title');
     */
    getString(name: string): Promise<string | undefined>;
    /** Retrieve a pre-cached GlobalSetting JSON object, note the object returned is not gauranteed to match the type
     * since under the hood it is simply using JSON.parse()
     * @returns a Promise resolving to the a JSON object of the GlobalSetting or undefined if not found
     * @example const myObject = await globalSetting.getJson('myObject.json');
     */
    getJson<T = any>(name: string): Promise<T | undefined>;
}
export interface ILabel extends IHasId<Guid>, IHasName {
    labelGroup: string;
    foreColor: number;
    backColor: number;
}
export interface ILabelsFacilityDefinition {
}
export interface ILabelsFacility extends IReadRecordFacility<Guid, ILabel> {
}
export interface IMembersFacilityDefinition {
    odataLegacy: {
        filter: string;
        select: string | string[] | (keyof OData.Legacy.Members)[];
        orderBy: string | string[] | (keyof OData.Legacy.Members)[];
    };
    offline: boolean;
}
export interface IMembersFacility extends IFacility {
    getAllODataLegacy(): Promise<OData.Legacy.Members[] | undefined>;
}
export interface ITaskType extends IHasId<Guid>, IHasName, IHasLabels {
    description: string | null;
}
export interface ITaskTypesFacilityDefinition {
}
export interface ITaskTypesFacility extends IReadRecordFacility<Guid, ITaskType> {
}
export declare enum TaskStatus {
    Unapproved = 0,
    Approved = 1,
    Started = 2,
    Paused = 3,
    FinishedComplete = 4,
    FinishedIncomplete = 5,
    Cancelled = 6,
    FinishedByManagement = 7,
    ApprovedContinued = 8,
    ChangeRosterRequested = 9,
    ChangeRosterRejected = 10,
    ChangeRosterAccepted = 11,
    FinishedBySystem = 12
}
export interface ITaskReadOnly extends IHasId<Guid> {
    readonly originalOwner: Guid;
    readonly scheduledOwner?: Guid;
    readonly rosterId?: number;
    readonly bucketId?: number;
}
export interface ITaskReadWrite<TContext = any> extends IHasLabels {
    /** Name of the task */
    name: string;
    /** Description of the task */
    description?: string;
    /** The status of the task */
    status: TaskStatus;
    /** Owner of the Task */
    ownerId: Guid;
    /** Site of the Task */
    siteId?: Guid;
    /** Asset attached to the Task */
    assetId?: number;
    /** Level of the task */
    level?: number;
    /** Order of the task related to other tasks */
    sortOrder?: number;
    /** The tasks context which is a user defined JSON object */
    context?: TContext;
}
export interface ITaskUpdateOptions {
    /**
     * `current` will use the current User.
     * `original` will use the original owner of the Task.
     * `parent` will look up the current users parent in the hierarchy.
     */
    ownerId: Guid | `current` | `original` | `parent`;
    /** Length in days can be a number, 1 means it ends at the end of the shift it is scheduled on */
    lengthInDays?: number | {
        endOfMonth: number;
    } | {
        startOfMonth: number;
    };
    /** Add labels via the Task Type Label Groups.
    @example
    If a Task Type has a "Dispatch 1" label which is in the "Dispatchable" Label Group then specifying
    labelsViaGroup: ["Dispatchable"]
    will attach the "Dispatch 1" label to the Task
    */
    labelsViaGroup?: Guid[];
}
export interface ITaskWriteOnce {
    /** The Task Type Id */
    taskTypeId: Guid;
    /** When the Task should start
     * @default Immediately
    */
    startTime?: Moment | Duration;
    /** When the Task should be considered Late to start */
    lateAfter?: Moment | Duration;
    /** Whether the Task should notify the user
     * @default false
    */
    notifyUser?: boolean;
    /** Number of seconds the task is estimated to take
     * @default from TaskType
     */
    estimatedDuration?: number;
    /** Number of seconds a dispatched task is expected to take before it is started
     * @default from TaskType
    */
    estimatedSecondsToStart?: number;
}
export declare type ITaskWithContext<TContext = any> = Readonly<ITaskReadOnly> & ITaskReadWrite<TContext> & Readonly<ITaskWriteOnce>;
export declare type ITask = ITaskWithContext<any>;
export declare type ICreateTaskRequest<TContext = any> = ITaskReadWrite<TContext> & ITaskUpdateOptions & ITaskWriteOnce;
export declare type IUpdateTaskRequest<TContext = any> = IHasId<Guid> & Partial<ITaskReadWrite<TContext>> & Partial<ITaskUpdateOptions>;
export interface ITasksFacilityDefinition {
}
export interface ITasksFacility extends IOfflineFacility<any, ICreateTaskRequest, IUpdateTaskRequest> {
    get<TContext = any>(id: Guid): Promise<ITaskWithContext<TContext>>;
    /** Create a new Task with a Task Definition.  The returned Task will have a unique id, however it will be persisted to the Server in the background */
    create<TContext = any>(definition: ICreateTaskRequest<TContext>): Promise<IOfflineCreateResult<ITaskWithContext<TContext>>>;
    /** Update an existing Task with a partial Task Definition */
    update<TContext = any>(definition: IUpdateTaskRequest<TContext>): Promise<IOfflineUpdateResult<ITaskWithContext<TContext>>>;
}
export interface IWorkingDocumentContext {
    /** The id of the checklist that is running */
    readonly checklistId: string;
    /** The working document id  */
    readonly workingDocumentId: string;
    /** MemberId of the reviewer */
    reviewerId: string;
    /** MemberId of the reviewee */
    revieweeId: string;
    /** Arguments passed into the checklist */
    args: any;
}
export interface IWorkingDocumentFacility extends IFacility {
    /** Explicitly set when this checklist will expire and be cleaned up */
    setExpiresAt(expiresAt: string | Date | Moment): Promise<void>;
    /** Explicitly set how long before this checklist will be expied and cleaned up */
    setExpiresDuration(duration: string | Duration): Promise<void>;
    /** Get when this checklist will be expired and due for cleanup */
    getExpiresAt(): Promise<Moment>;
}
export interface IFacilities {
    designDocument(definition?: IDesignDocumentFacilityDefinition): IDesignDocumentFacility;
    globalSettings(definition?: IGlobalSettingsFacilityDefinition): IGlobalSettingsFacility;
    labels(definition?: ILabelsFacilityDefinition): ILabelsFacility;
    members(definition?: IMembersFacilityDefinition): IMembersFacility;
    tasks(definition?: ITasksFacilityDefinition): ITasksFacility;
    taskTypes(definition?: ITaskTypesFacilityDefinition): ITaskTypesFacility;
    workingDocument(): IWorkingDocumentFacility;
}
export declare type FacilityType = keyof IFacilities;
declare const Facility: IFacilities;
export { Facility };
export interface IPresentArg {
}
export declare function Present(...args: IPresentArg[]): void;
