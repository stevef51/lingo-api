"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Present = exports.Facility = exports.TaskStatus = void 0;
var TaskStatus;
(function (TaskStatus) {
    TaskStatus[TaskStatus["Unapproved"] = 0] = "Unapproved";
    TaskStatus[TaskStatus["Approved"] = 1] = "Approved";
    TaskStatus[TaskStatus["Started"] = 2] = "Started";
    TaskStatus[TaskStatus["Paused"] = 3] = "Paused";
    TaskStatus[TaskStatus["FinishedComplete"] = 4] = "FinishedComplete";
    TaskStatus[TaskStatus["FinishedIncomplete"] = 5] = "FinishedIncomplete";
    TaskStatus[TaskStatus["Cancelled"] = 6] = "Cancelled";
    TaskStatus[TaskStatus["FinishedByManagement"] = 7] = "FinishedByManagement";
    TaskStatus[TaskStatus["ApprovedContinued"] = 8] = "ApprovedContinued";
    TaskStatus[TaskStatus["ChangeRosterRequested"] = 9] = "ChangeRosterRequested";
    TaskStatus[TaskStatus["ChangeRosterRejected"] = 10] = "ChangeRosterRejected";
    TaskStatus[TaskStatus["ChangeRosterAccepted"] = 11] = "ChangeRosterAccepted";
    TaskStatus[TaskStatus["FinishedBySystem"] = 12] = "FinishedBySystem";
})(TaskStatus = exports.TaskStatus || (exports.TaskStatus = {}));
function Present() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    // This is never called, Lingo transpiler will call correct method at runtime
}
exports.Present = Present;
